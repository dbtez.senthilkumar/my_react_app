FROM node as build

USER root
RUN apt-get update && apt-get -y install sudo
RUN apt-get install curl
RUN apt-get install net-tools
RUN apt-get -y install --reinstall systemd
RUN apt-get -y install ca-certificates curl gnupg lsb-release
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
#RUN apt-get -y install docker-ce docker-ce-cli containerd.io
RUN apt-get -y install iputils-ping
RUN useradd -m docker && echo "docker:docker" | chpasswd && adduser docker sudo
WORKDIR .
RUN ls
RUN pwd
COPY . .
#COPY package*.json ./
#COPY cmd.sh ./
#COPY src .
#COPY public .
RUN ls
RUN pwd

RUN npm install
RUN npm install --save react@latest
RUN npm install react-scripts@latest -g
RUN npm run build

RUN ls
RUN pwd

# Prepare nginx
FROM nginx
COPY --from=build /build /usr/share/nginx/html
#RUN rm /etc/nginx/conf.d/default.conf
#COPY /nginx/nginx.conf /etc/nginx/conf.d/
RUN pwd
RUN ls
run ls /usr/share/nginx/html
run ls /etc/nginx/conf.d/
# Fire up nginx
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
#CMD ["node", "./index.js"]
#CMD ["npm", "start"]
#MD ["npm", "run", "build"]
